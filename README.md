# Pal

Description, specification and tools for Pal, the pithy arborescent language

# Goals

This markup language aims to provide a mean to associate data while:
 - staying human friendly for read and write activities
 - offering fully extensible tree structure facility with a minimal syntax
 - providing an easy to parse solution that can be trivialy implemented anywhere

# Overview

Basicaly *Pal* explicit each node depth, either with an absolute value, or
relatively to the previous node.

Pal is a strict superset of Pan, the pithy arborescent notation, molded on the
same idea to explicit node depths, but only allowing absolute values. Any valid
Pan structure is a valid Pal structure, and any Pal structure can be reduced to
a Pan structure.

## A First Example: an Absolute Single Node of Third Depth
Let's start with a simple example using absolute depth notation:

```pal
3 value in a node of third depth
```

Pal deals with two kinds of entity:
 - node statement, named "hem"
 - node content, named "nub" 

So here `3` is a hem that states "create a node with a depth of 3"
And `value in a node of third depth` is the occurence of a nub, that is
associated to the just created node.

Note that this is stricktly equivalent with this snippet:
```pal
1 2 3 value in a node of third depth
```

This expresses the same as the following javacript snippet:
```js
[null, [null, ["value in a node of third depth"]]]
```

A somewhat equivalent valid JSON object would be something like:
```json
{depth1: {depth2: { depth3: "value in a node of third depth" } } }
```

And a somewhat equivalent document in XML would be:
```xml
<?xml version="1.0"?>
<depth1><depth2><depth3>value in a node of third depth</depth3></depth2></depth3>
```

Terseness apart, the two remarkable facilities of the Pal version are:
  1. anonymous nodes are possible
  2. a node can be closed without any explicit closure mark up, because a node
     ends with a new node of lower or equal depth or when the data stream ends

So the shortest Pal tree is `0` which is somewhat equivalent with a javascript
`{null}` or `[null]`. Indeed, even if it is empty, a Pal node always has a nub.

Also note that only strictly increasing series of absolute depth can be reduced
to their last term. So `1 2 3 3 nub` can be reduced to `3 3 nub`, and 
`1 2 3 4 2 1 2 3 nub` can be reduced to `4 2 1 3 nub`, but not further.
Indeed, any explicit occurence of a lower or equal depth implies to create a 
node with this depth, while every intermediate depth – if any – is only closed.
```js
// 1 2 3 3 nub
// 3 3 nub
[null, [null, [null], ["nub"]]]
// 1 2 3 4 2 1 2 3 nub
// 4 2 1 3 nub
[null, [null, [null, [null]], null], null, [["nub"]]]
```

## Anonymously Nested Node

The two previously mentionned facilities are especially handy when combined
to express anonymously nested nodes. An example will express it more
clearly. Let's say we want to express an equivalent of this xml snippet:
```xml
<life quality="great">That's it!</life>
```

In XML, "quality" is generaly named an attribute of the "life" node. You could
also express the same arborescent structure in XML as:
```xml
<life>
  <attributes>
    <quality>
      great
    </quality>
  </attributes>
  <content>
    That's it!
  </content>
</life>
```

But XML's attributes allows to make the structure a bit terser through an
anonymous nested node: you don't explicitely states that "quality" is a
subnode of the "attributes" subnode of life, but that's what the syntax
essentialy means. Also in the last XML snippet, the logic of subdivizion was
pushed a bit further by putting the previous content of the node in yet an other
explicitely named node. This wouldn't be technically mandatory from XML's point of
view but this makes structure clearer and is closer to what Pal let you simply
achieve, while sparing you the worry of naming your nodes.


So, one way you would encode that in Pal would be:
```pal
1 life 3 quality 4 great 2 That's it!
```

Nodes can also be declared with a depth relative to currently ongoing depth,
with a default value of one. So using relative depth, the snippet could be
expressed as :
```pal
:0 life :2 quality :1 great -2 That's it!
```

Let's decompose this expression :
  - `:0` means "create a node of current depth plus zero", remember: default depth is one
  - `:2` means "create a node of current depth plus two"
  - `:1` means "create a node of current depth plus one"
  - `-1` means "create a node with the current depth minus one"

Graphically, this tree could be rendered as
```
└┬─ life
 └┬┬── quality
  │└──── great
  └─── That's it!
```

A similar structure could be expressed in javascript as:
```javascript
["life", [null, [ "quality", ["great"] ],  ["That's it!"] ] ]
```

## More Ways to Declare Nodes

So far we saw node declarations using explicit node depth, either with absolute
or relative value. Pal allows to also declare nodes with a few other prefix to
integers.

### Multiple Copies

The `*` prefix operator means "create multiple copy of the following node
including its subnodes, using the current depth".

So `*3 node` is equivalent to the javascript:
```javascript
[ "node", "node", "node" ]
```

Multiplied nodes include their subnodes, so `*3 term :1 value` is somewhat like
the javascript:
```javascript
[ "term", ["value"], "term", ["value"], "term", ["value"] ]
```

As usual the node ends when a node is created with a lower or equal depth.
So `*5 :1 1 Sixth` is somewhat like the javascript:
```javascript
[ [null], [null], [null], [null], [null], "Sixth" ]
```

When you multiply by zero, you except zero as a result, and this is what you get
with Pal. That means that any node under a `*0` branch will be ignored. So for
example `*1 start *0 anything :1 here :2 will 4 be 8 ignored 1 end`
expresses something like the following javascript:
```javascript
["start", "end"]
```


### Depth Tabs and Tags

As an alternative prefix to `:`, it's possible to use a bar `|`.
They both have the same semantic so `:3 nub` and `|3 nub` stands for the same
node declaration. However the later also indicate to bookmark its depth, which
enable any later node declaration to reuse this same depth level, thus
effectively creating a sibling node. The depth of multiplied node declaration is
also added in this bookmark. So `|` and `*` are the tab operators.

```
Note that Pal *doesn't* define a non-tab multiplication declaration such as `⋅`.
```

The bookmark memorizes a depth value until a node of lower value is reached.

The dual of the tab signs is the tag sign `#`, which indicates the
depth to use within those bookmarked, counting backward.

So the two following snippet are equivalents:

```pal
*1 depth₁ :1 depth₂ :1 depth₃ :1 depth₄ 2 depth₂ 1 depth₁
```

```pal
*1 depth₁ |1 depth₂ :1 depth₃ |1 depth₄ #2 depth₂ #2 depth₁
```

Indeed, `#2` tells to recuperate the second last element in the depth bookmark,
which at this point can be represented as the array `[1, 2, 4]`. So the new node
is assigned a depth of 2 and the bookmark array is reduced to `[1, 2]`.
So the next `#2` node declaration takes the second last depth remaining in the
bookmark, reducing it at the same time to `[1]`.

Note that the bookmark starts empty and that calling an index greater than its
size is invalid. So `#1 root`, `:0 one #1 two` and `*1 one #2 two` are all
invalids as is.

Finaly an tags with an index of zero is a way to comment a single node.
So `*1 start #0 ignored :1 continue 1 end` expresses something like the
following javascript:
```javascript
["start", ["continue"], "end"]
```

### Standalone Prefixes

When they are not immediately followed by an integer, prefixes assume a value of
one. So to state it explicitely:
  - `:` is equivalent to `:1`
  - `-` is equivalent to `-1`
  - `*` is equivalent to `*1`
  - `|` is equivalent to `|1`
  - `#` is equivalent to `#1`

So this snippet:
```pal
1 life 3 quality 4 great 3 action 4 love 3 aim 4 haromyn 2 That's it!
```

Is equivalent with:
```pal
*1 life |1 :1 quality :1 great -1 action :1 love -1 aim :1 harmony #1 That's it!
```

Which is itself equivalent with:
```pal
*life|:quality:great-action:love-aim:harmony#That's it!
```


## Spaces and Indentation

Pal default behavior regarding node values is to pack any space into a single
one and completely remove initial and final ones. This is called lax values.

So the previous snippet could also be written:
```pal
* life
  |
    : quality: great
    - action: love
    - aim: harmony
  # That's it!
```

Or to make it looks more harmonious at the cost of an extra anonymous empty
node:
```pal
* life
  |::
    - quality: great
    - action: love
    - aim: harmony
  # That's it!
```

With absolute depth notation, this would be written:
```pal
1 life 2 3 4 3 quality 4 great 3 action 4 love 3 aim 4 haromyn 2 That's it!
```

Note that even absolute depth nodes are space insensitives as far as no node
declaration is involved. But at least one space must separate two consecutive
absolute empty node declarations. So the following is completely legit:
```pal
1life2 3 4 3quality4great3action4love3aim4haromyn2That's it!
```

Although, as already seen, rising series are pleonasmics and this could be
reduced down to:
```pal
1life4 3quality4great3action4love3aim4haromyn2That's it!
```

The relative notation however don't require spaces between node declarations,
so it stays very compact while being more human readable:
```pal
*life|::-quality:great-action:love-aim:harmony#That's it!
```

To further expand readability, the following longer monoline form is preferable:
```pal
*life|:: -quality:great -action:love -aim:harmony # That's it!
```

## Pal's Literal

So far so good, but at some point you might want to include some text that would
collide with Pal's vocabulary. You might also want to not strip initial and
final spaces. For this cases, Pal uses a balanced square brackets literal
operator, optionally doubled and named. The encapsulated string is a raw value
gob.

Let's say that this time we want to add to our snippet:
  - the string "  say: hello!  ", preserving the spaces,
  - and the string "42" within a delimiter named  "answer".
```pal
* life
  |::
    - action: [  say: "Hello, world!"  ]
    - meaning: [answer[42]answer]
  # That's it!
```

Or equivalently:
```pal
*life|::-action:[  say: "Hello,world!"  ]-meaning:[answer[42]answer]#That's it!
```

Any sequence of characters can be putten between the two opening brackets,
except the brackets themselves and a single hash sign. The string literal will
only end when the same inner pattern is matched embeded in closing brackets:

```pal
* array: [js[ [[], 1, {o:2}, [3]] ]js]
```

This facility is only a switch: it's content is joined to the rest of the
surounding value gobs as usual.  So this snippet is possible:
```pal
* text: In javascript the array `[js[[[[], 1, {o:2}, [3]]]]js]` is valid.
```

But this equivalent snippet is probably more readable:
```pal
* text: [~[In javascript the array `[[], 1, {o:2}, [3]]` is valid.]~]
```

Pal is very liberal regarding gob names. Constructs like the following are
possible although it's probably not the most readable choice.
```pal
* text: [:-*#|[In Pal `::node: [|#*-:[value]|#*-:]` is a valid construct.]:-*#|]
```

A value gob can be turned into a comment by surounding it with double brackets
whose name is composed of any number of space and a single hash sign. The name
must still match the exact same pattern in the closing delimiter.

So in the folling snippet:
```pal
* text: This is [ # [really] # ] [#[extremly]#] simple.
```
The subnode of `text` has the value `This is simple`.


Note that Pal *doesn't* support balanced symbol matching. The following snippet
*doesn't* respect the syntax of Pal literals:
```pal
::text: [([Invalid attempt of using balanced parentheses in Pal])]
```

This design choice is done to keep Pal implementation as simple as possible for
such a general structuring language, and ease regexp matchnig.


## Further Considerations

That's almost everything one need to know about Pal. Here are the few more points one might know:
  - data outside of node values are ignored, namely:
    - anything coming before a first node was declared
    - anything coming between a tree closure and a new node declaration
  - a relative node declaration can not set a value below zero
