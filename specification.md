# Specification of Pal

0. rim bounds are [paired brackets][brackets]
0. a rim name is any string of any glyph except its bounds
0. an opening raw rim:
   0. starts with an opening rim bound
   0. is optionnally followed by a rim name, ended with a new opening rim bound
0. a closing raw rim:
   0. starts with a closing rim bound matching the opening one
   0. if named, lasts with the matching name, ended with a new closing rim bound
0. every occurence of an opening raw rim must be balanced with a closing one
0. a raw value is any string that:
   0. starts with a raw rim
   0. is followed by any string which doesn't match the closing raw rim
   0. ends with the matching closing raw rim
0. rims are not a part of the raw value they wrap
0. a raw value can contain any string forming an opening rim
0. a raw value can contain any string forming a closing rim except its own
0. there are five node declaration operations
   0. dig
   0. ebb
   0. put
   0. pit
   0. tag
0. an act is a string serving as prefix operator for an operation
0. a depth value is an element of a well-ordered set
0. an integer is any continuous sequence containing only digits
0. an integer stands for its custom value
0. a hem is either
   0. an integer alone
   0. an act and its integer operand
   0. an act alone
0. a hem ends as soon as a non-digit glyph occurs
0. an act alone is equivalent with an act with an implicit one as operand
0. a lax value is any string except raw rims and a hems
0. a lax value compact any kind of space sequence into a single simple space
0. a lax value eliminate any starting and ending space
0. freely spaced means that any space appearing in the string is ignored
0. a commented raw value uses a name whose first non-blank glyph is a tag act
0. a nub is a sequence of lax and raw values
0. a nub value concatenates its lax and raw values, except commented ones
0. a node is a structure with a depth value, a nub and any number of subnodes
0. a hem occurence declares a node creation
0. anything coming after a node hem constitutes its nub
0. a nub lats until a new hem occurs
0. a nub immediately following a hem constitutes its nub
0. until explicitely set otherwise, nodes are declared with a depth of one
0. the tab stack is an ordered structure of depth values
0. a gap is the difference between a new node depth and the current one
0. a positive gap declares as many due nested nodes to make the node that deep
0. a negative or null gap closes all opened nesting nodes with a lower or equal depth
0. a negative or null gap doffs any value lower than the declared depth from the tab stack
0. an integer occurence creates a node of the depth specified by the integer
0. an integer occurence closes as any node of lower or equal depth value
0. a dig creates a node of the current depth plus its operand
0. an ebb creates a node of the current depth minus its operand
0. an ebb can not set a node depth under zero
0. a put creates as many copy as the value specified of the coming node branch
0. a put sets its first node with the current depth
0. a pit creates a node of the current depth plus its operand
0. pit and put are tab operations
0. a tab operation appends its operand to the tab stack
0. a tag whose operand is zero indicates to ignore this single node
0. a tag operand can not be greater than the tab stack size
0. a tag taps a depth value off the tab stack with its operand as reverse index[^1]
0. a tag creates a node using the taped value as node depth
0. a tag creates a node using its operand as reverse index of tab stack item
0. strings within raw values are never interpreted as node declaration[^2]
0. a node ends when:
   0. the data stream ends
   0. a new node of lower depth is declared
   0. a node of zero depth is declared, either through relative or absolute value
0. a node contains any node declared after itself until it ends
0. a node is allways enclosed in as many node its depth indicates
0. a tree declaration is a sequence of node declarations
0. a tree declaration ends when
   0. the data stream ends
   0. a node of zero depth is declared, either through relative or absolute value
0. data after a zero depth node are not part of the data stored within the tree
0. a tree declaration starting with a zero depth node represents an empty tree
0. data occuring outside a node declaration are not part of any tree
0. a unique pair of bounds is used consistently through a whole tree declaration
0. a unique digit set is used consistently through a whole tree declaration
0. a unique set of acts is used consistently through a whole tree declaration
0. a data stream may contains several trees

## Actual Hems

So far, the specification purposefully doesn't explicit which glyph sequences
should be used for acts, digits and bounds. This allows to reuse this
specification with any string set to represent each hem.

However, Pal does specify a glyph set covering each of these items. A pal parser
must support them.

The actual hems set was determined as a balancing trade-off
between various criteria, including human readability and accordance with
existing practices:
- the opening rim bound is `[`
- the closing rim bound is `]`
- the dig act is `:`
- the ebb act is `-`
- the put act is `*`
- the pit act is `|`
- the tag act is `#`
- digits are "0123456789"

Integer interpretation use the usual decimal positional notation.

## A Valid Example
Thus, one way the Pal glyph set could be declared in a Pal tree is:
```pal
* glyph|
  # bound::
     - opening: [~[[]~]
     - closing: [~[]]~]
  # act::
     - dig: [:]
     - ebb: [-]
     - put: [*]
     - pit: [|]
     - tag: [#]
  # digits: [0123456789]
```

Note how all glyphs that are part of Pal's glyph set are
embeded in raw rims, the rim bounds being themselves between named raw rims.

The opening square bracket could also be coded with the shorter string
`[[[]]`.

But a closing bracket can not be encoded `[[]]]`. The first part `[[]]`
is a valid way to explicitely express an empty string. But the last `]` would
be invalid, as all square brackets outside a raw value must be balanced. Indeed
a `]` alone is not a valid way to code a closing square bracket as a lax value.


Here is a valid interpretation of the begining of the example:
- `* glyph`
  - create one copy of the following branch
  - set current depth as depth to the newly created node, here as the default `1`
  - set `glyph` as current node nub
  - add `1` on the tab stack
- `|`
  - create a node
  - set current depth plus one as depth to the newly created node (`2`)
  - add `2` on the tab stack
- `# bound`
  - tap the last value stored on the tab stack (`2`)
  - close any node lower or equal to the taped depth (`2`)
  - create a node
  - set taped depth as depth to the newly created node (`2`)
  - set `bound` as current node nub
- `:`
  - create a node
  - set current depth plus one as depth to the newly created node (`3`)
- `:`
  - create a node
  - set current depth plus one as depth to the newly created node (`4`)
- `- opening`
  - close any node lower or equal to current depth minus one (`3`)
  - create a node
  - set current depth minus one as depth to the newly created node (`3`)
  - set `opening` as current node nub
- `:`
  - create a node
  - set current depth plus one as depth to the newly created node (`4`)
- `[` open a raw rim
  - `~` set this string as raw rim name
  - `[` terminate the opening raw rim
- append `[` to the current nub
- `]` consider closing the opened raw rim
  - `~` append this string as a possible closing raw rim name
  - `]` terminate the closing raw rim possibility, names match, close the raw value
- `- closing`
  - close any node lower or equal to current depth minus one (`3`)
  - create a node
  - set current depth minus one as depth to the newly created node (`3`)
  - set `closing` as current node nub
- `:`
  - create a node
  - set current depth plus one as depth to the newly created node (`4`)
- `[` open a raw rim
  - `~` set this string as raw rim name
  - `[` terminate the opening raw rim
- `]` consider closing the opened raw rim
  - `]` terminate the closing raw rim possibility, names don't match
  - append `]` to the raw value
- `]` consider closing the opened raw rim
  - `~` append this string as a possible closing raw rim name
  - `]` terminate the closing raw rim possibility, names match, close the raw value

And so on.

# Notes and references

[brackets]: https://www.unicode.org/reports/tr9/#Paired_Brackets
[^1]: That is counting backward from the last item, so an operand of *one* stands
     for the last tab stack item, an operand of *two* stands for the second last
     tab stack item, and so on.
[^2]: That is, even when a valid node hem substring occur in these strings.
